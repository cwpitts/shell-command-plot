""" Generate a plot of your most common shell commands from a CSV!

Graphing based on these examples:

- https://www.python-graph-gallery.com/circular-barplot-basic
- https://www.python-graph-gallery.com/web-circular-barplot-with-matplotlib
"""
import os
import re
import sys
from argparse import ArgumentParser, Namespace
from collections import Counter
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def circle_plot(
    data: np.ndarray,
    labels: List[str],
    max_length: int=100,
    ylim_min: int=-50,
    cmap: str="viridis",
    label_padding: int=5,
    background_color: str="gray",
):
    """ Produce circular plot of data

    Args:
      data (np.ndarray): Data array to plot
      labels (List[str]): String labels for data
      max_length (int, optional): Maximum length of bars, defaults to 100
      ylim_min (int, optional):
        Minimum y-value of plot, used to tune how close the bottom of
        the bars are to each other, defaults to -50
      cmap (str, optional): Matplotlib colormap to use, defaults to 'viridis'
      label_padding (int, optional):
        Padding between labels and the end of bars, defaults to 5
      background_color (str, optional):
        Background color of plot, defaults to 'gray'
    """
    # Normalize data
    if not isinstance(data, np.ndarray):
        data = np.array(data)

    data_max: np.int64 = np.max(data)
    data_min: np.int64 = np.min(data)
    data_norm: np.ndarray = data.copy()
    data_norm: np.ndarray = (data_norm - data_min) / (data_max - data_min)

    # Compute bar characteristics
    bar_width: float = 2 * np.pi / len(data)
    bar_angles: np.ndarray = np.arange(1, len(data) + 1) * bar_width

    fig, ax = plt.subplots(subplot_kw={"projection": "polar"})

    if isinstance(cmap, str):
        cmap = plt.get_cmap(cmap)
    
    bars = ax.bar(
        bar_angles,
        data_norm * max_length,
        width=bar_width * 0.9,
        color=cmap(data_norm),
        zorder=10,
    )

    ylim_max: float = max_length * 2.3

    ax.set_ylim(ylim_min, ylim_max)
    ax.set_yticks([])
    ax.set_xticks([])

    for label, count, bar_angle, bar in zip(labels, data, bar_angles, bars):
        rot = np.degrees(bar_angle)
        if np.pi / 2 <= bar_angle <= 3 * np.pi / 2:
            rot += 180
            rot %= 360
            alignment = "right"
        else:
            alignment = "left"
        ax.text(
            bar_angle,
            bar.get_height() + label_padding,
            s=f"{label} - {count}",
            va="center",
            rotation=rot,
            rotation_mode="anchor",
            ha=alignment,
        )

    ax.grid(False)
    ax.set_facecolor(background_color)
    fig.set_facecolor(background_color)
    for spine in ax.spines.keys():
        ax.spines[spine].set_visible(False)

    return fig, ax


def parse_args() -> Namespace:
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("outfile", help="path to write image to")
    argp.add_argument("-n", help="take top n entries", default=15, type=int)
    argp.add_argument(
        "--background-color", "--bg", help="background color", default="gray"
    )
    argp.add_argument("--dpi", help="DPI to save with", default=300, type=int)
    argp.add_argument("--bbox", help="bounding box", default="tight")
    argp.add_argument("--cmap", help="colormap to use for bars", default="viridis")
    argp.add_argument(
        "--strip-sudo",
        help="strip sudo from command",
        default=False,
        action="store_true",
    )

    return argp.parse_args()


if __name__ == "__main__":
    args: Namespace = parse_args()

    cmds: List[str] = []
    line: str = sys.stdin.readline()
    while line:
        line = sys.stdin.readline().strip()
        matches = re.match(r"\s*[0-9]+  (.*)", line)
        if not matches:
            continue
        parts: List[str] = matches.group(1).split(" ")
        cmd: str = parts.pop(0)
        while re.match(r"[a-zA-Z_][a-zA-Z_0-9]*=.* ", cmd) or (
            args.strip_sudo and cmd == "sudo"
        ):
            cmd = parts.pop(0)
        if cmd:
            cmds.append(cmd)

    df: pd.DataFrame = pd.DataFrame(Counter(cmds).items(), columns=["command", "count"])

    n_largest: pd.DataFrame = df.nlargest(args.n, ["count"])
    fig, ax = circle_plot(
        n_largest["count"],
        n_largest["command"],
        background_color=args.background_color,
        cmap=args.cmap,
    )
    fig.savefig(args.outfile, dpi=args.dpi, bbox_inches=args.bbox)
