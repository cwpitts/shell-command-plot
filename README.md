# Shell Command Plot

A small Python tool to visualize your shell command history.

## Usage
The tool is designed to be used in a streaming fashion:

```
history | python3 main.py foo.png
```

There are plenty of options:

```
usage: main.py [-h] [-n N] [--background-color BACKGROUND_COLOR] [--dpi DPI] [--bbox BBOX] [--cmap CMAP]
               [--strip-sudo]
               outfile

positional arguments:
  outfile               path to write image to

optional arguments:
  -h, --help            show this help message and exit
  -n N                  take top n entries
  --background-color BACKGROUND_COLOR, --bg BACKGROUND_COLOR
                        background color
  --dpi DPI             DPI to save with
  --bbox BBOX           bounding box
  --cmap CMAP           colormap to use for bars
  --strip-sudo          strip sudo from command
```

The `--strip-sudo` flag bears some explaining: a *nix command-line
environment will often have many invocations of `sudo` followed by a
command; while this could still be an interesting part of the
visualization, it's also interesting to see exactly which commands are
being invoked behind `sudo`. So, this flag will instead take the next
part of the command string, which is the actual command beind invoked
with elevated privileges.
